# NEXUS AND AUTOMATION

## Table of Contents
- [Project Description](#project-description)

- [Installation](#installation)

- [Screenshots](#sscreenshots)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description
* Setup  Nexus and created repostories for 2 different projects 

* Installed Nexus on a server 

* Created a new npm hosted repository with a new blob store 

* Created Nexus user for the project 1 user to have access to the npm repo 

* Tested if porject 1 user has correct access configured so i built and published a nodejs tar package to the npm repo 

* Created maven hosted repository for project 2 user 

* Created user for project 2 

* Created new user  that has access to both repositories 

* On cloud server on digitalocean i used Nexus Rest API to fetch the download URL info for the NodeJS app artifact 

* Executed Command which fetches artifact itself with the download URL 

* I extracted the artifact and ran it on cloud server 

* Automate: Wrote a script that fetches the artifact from npm repo 

* Automate: This scripts extracts the Artifact 

* Automate: script runs the Artifact   

- wget -c https://download.sonatype.com/nexus/3/nexus-3.42.0-01-unix.tar.gz

## Installation

* $ sudo apt install openjdk-8-jre-headless

* $ sudo apt install net-tools 

* $ wget -c https://download.sonatype.com/nexus/3/nexus-3.42.0-01-unix.tar.gz

* $ sudo apt install npm 

## Screenshots 

![Cloud Server](/images/01_Cloud_server_Creation.png)
> Displays Cloud server creation on DigitalOcean 

![Connecting to Cloud Server](/images/02_Connecting_to_Cloud_Server.png)
> Connecting to Cloud Server 

![Nexus Downloaded](/images/03_Nexus_Downloaded.png)
> Nexus Downloaded on Cloud Server 

![Nexus Extracted](/images/04_Nexus_extracted.png)
> Untar Nexus 

![Nexus User on Server](/images/05_Nexus_user_on_server.png)
> Nexus User on Server 

![Changing Ownership](/images/06_changing_ownership.png)
> Changing Ownership 

![Finding public ssh key](/images/07_finding_public_ssh_key.png)
> Finding Publice Ssh Key 

![Connecting to Nexus Server](/images/09_Connecting_to_nexus_user_on_server.png)
> Connecting to Nexus Server 

![Giving Access](/images/10_giving_nexus_user_access.png)
> Giving Nexus User Access in config file 

![Nexus Configuration](/images/11_nexusrc_configuration.png)
> Nexusrc Configuration 

![Installing Java](/images/12_installing_java.png)
> Installing Java 

![Net-tools Installation](/images/13_net_tools_installed.png)
> Net-tools Installation 

![Starting Nexus on Server](/images/14_starting_nexus_on_server.png)
> Starting Nexus on Server 

![Checking Port & PID](/images/15_checking_for_port_and_PID.png)
> Checking Port & PID 

![Nexus UI](/imaeges/17_nexus_UI.png)
> Nexus UI

![Admin Password](/images/18_admin_password.png)
> Nexus Admin Password Location 

![NPM Repo Creation](/images/19_npm_repo_creation.png)
> NPM Repo Creation 

![Blob Store Creation](/images/20_blob_store_created.png)
> Blob Store Creation 

![User Creation](/images/21_user_creation.png)
> User Creation on Nexus 

![Creating Roles](/images/22_creating_roles.png)
> Creating Roles in Nexus 

![Assign User to Role](/images/23_assigning_user_to_role.png)
> Assigning Nexus User to Role 

![Nodejs Install](/images/24_nodejs_install.png)
> Nodejs Install 

![NPM install](/images/25_npm_installed.png)
> NPM installed 

![NPM test](/images/26_npm_test.png)
> NPM test 

![Tar Artifact Created](/images/27_tar_artifact_created.png)
> TAR Artifact Creation

![Configure npmrc](/images/28_configuring_npmrc.png)
> Configure npmrc 

![Enabling npm realm](/images/29_enabling_npm_bearer_realm.png)
> Enabling npm bearer realm

![Publishing nodejs Application](/images/30_pushing_nodejs_application_to_nexus.png)
> Publish nodejs Application 

![Node Application on Nexus](/images/31_nodejs_app_on_nexus.png)
> Nodejs Application on Nexus 

![New Maven Host](/images/32_new_maven_host.png)
> New Maven Host on Nexus 

![Java User Role](/images/33_creating_role_for_java_user.png)
> Java User Role on Nexus 

![Java User Assigned Role](/images/34_assigning_javauser_to_role.png)
> Java User Role on Nexus 

![Plugin Insert](/images/35_plugin_insert.png)
> Inserting Plugins in build gradle 

![Gradle Properties Creation](/images/36_gradle_properties.png)
> Gradle Properties creation 

![Gradle Test](/images/37_gradle_test.png)
> Gradle Test 

![Built Java Artifact](/images/38_built_java_artifact.png)
> Built Java Artifact 

![Java Artifact on Nexus](/images/39_java_artifact_on_nexus.png)
> Java Artifact on Nexus 

![Created User on Server](/images/40_creating_new_user_on_server.png)
> Creating New User on Server 

![Creating User on Nexus](/images/41_creating_user_on_nexus_with_access_to_both_repos.png)
> User with access to both Repos 

![Artifact URL Info](/images/42_fetching_url_info_for_nodejs_artifact.png)
> Artifact URL Info 

![Downloaded Artifact on Server](images/43_downloaded_artifact_on_server.png)
> Download Artifact on Server

![Nodesjs on Server](/images/44_nodejs_on_server.png)
> Nodejs on Server

![Node Port](/images/45_port_in_which_node_app_is_listening.png)
> Node Port 

![Application UI](/images/46_application_ui.png)
> Node Application ui

![Bash Script 1](images/47_bash_script_1.png)
> Bash Script 1 

![Bash Script 2](images/48_bash_script_2.png)
> Bash Script 2 

## Usage 

* npm start 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/cloud-and-infrastructure-as-service-basics.git

2. Create new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review 

## Tests

Test were run using:

- $ ./gradlew test 
- $ mvn test
- $ npm test

## Questions

feel free to contact me for further questions via:

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com 

## References

https://gitlab.com/omacodes98/nexus-and-automation

## License

The MIT License 

for more information you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji. 