#!/bin/bash 
url="$(curl -u innocent:ino -X GET 'http://159.65.89.76:8081/service/rest/v1/components?repository=npm-snapshots&sort=1.0.0')"
read -p " Do you want nodejs application downloaded from nexus (y/n) " answer

if [[ $answer == "y" ]]
then
        echo " verifying if file nodejs application exists on Nexus.... "
        sleep 3
        if [[ "$url" == *"npm-snapshots"* ]]
        then
                echo " nodejs app is available on Nexus "
                read -p " Do you want to Download the artifact (y/n) " answer2
                if [[ "$answer2" == "y" ]]
                then
                       echo " Downloading node_application.tgz ... "
                       sleep 2
                       curl -u innocent:ino -X GET http://159.65.89.76:8081/repository/npm-snapshots/bootcamp-node-project/-/bootcamp-node-project-1.0.0.tgz > node_application.tgz
                       read -p " Do you want the appliction extracted (y/n) " extracted
                       if [[ $extracted == "y" ]]
                       then
                               echo " extracting node_application.tgz "
                               tar -xzvf node_application.tgz
                               read -p " Start node application (y/n) ? " start_application
                               if [[ $start_application == "y" ]]
                               then
                                       echo "moving to package"
                                       cd ./package
                                       sleep 1
                                       echo " installing dependencies "
                                       npm install
                                       echo " starting application " 
                                       npm start &
                              else
                                       echo "Quiting .."
                              fi



                       else
                               echo " Quiting .... "
                               exit
                       fi

                        echo " Downloading..... "
                else
                        echo " exiting script .... "
                        exit
                fi
        else
                echo " nodejs app is not available on Nexus "
        fi
elif [[ $answer == "n" ]]
then
       echo " Quitting "
        exit

else
        echo " incorrect character "
fi

